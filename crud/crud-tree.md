<script>
export default {
  data(){
    return {
       data: [
        {
          id: 0,
          event: '事件1',
          timeLine: 50,
          icon: 'el-icon-time',
          comment: '无'
        },
        {
          id: 1,
          event: '事件1',
          icon: 'el-icon-bell',
          timeLine: 100,
          comment: '无',
          children: [
            {
              id: 2,
              event: '事件2',
              icon: 'el-icon-star-on',
              timeLine: 10,
              comment: '无'
            },
            {
              id: 3,
              event: '事件3',
              timeLine: 90,
              comment: '无',
              children: [
                {
                  id: 4,
                  event: '事件4',
                  timeLine: 5,
                  comment: '无'
                },
                {
                  id: 5,
                  event: '事件5',
                  timeLine: 10,
                  comment: '无'
                },
                {
                  id: 6,
                  event: '事件6',
                  timeLine: 75,
                  comment: '无',
                  children: [
                    {
                      id: 7,
                      event: '事件7',
                      timeLine: 50,
                      comment: '无',
                      children: [
                        {
                          id: 71,
                          event: '事件71',
                          timeLine: 25,
                          comment: 'xx'
                        },
                        {
                          id: 72,
                          event: '事件72',
                          timeLine: 5,
                          comment: 'xx'
                        },
                        {
                          id: 73,
                          event: '事件73',
                          timeLine: 20,
                          comment: 'xx'
                        }
                      ]
                    },
                    {
                      id: 8,
                      event: '事件8',
                      timeLine: 25,
                      comment: '无'
                    }
                  ]
                }
              ]
            }
          ]
        }
      ],
      option: {
        headerAlign: 'center',
        align: 'center',
        border: true,
        index: true,
        defaultExpandAll:true,
        column: [
          {
            label: '事件',
            prop: 'event',
            align: 'left',
            width: 200
          },
          {
            label: 'ID',
            prop: 'id',
            dicData: [{
              label: '零',
              value: 0
            }, {
              label: '一',
              value: 1
            }, {
              label: '二',
              value: 2,
            }]
          },
          {
            label: '时间线',
            prop: 'timeLine'
          },
          {
            label: '自定义图标',
            prop: 'icon',
            slot: true,
          },
          {
            label: '备注',
            prop: 'comment'
          }
        ],
      }
  }
  }
}
</script>

# 表格树
配置rowKey主键属性(默认为id),并且使用ele2.8.2+以上版本

:::tip
 1.0.6+
::::
## 普通用法
:::demo `defaultExpandAll`属性是否全部展开,其他用法和普通crud一致,一定要配置`rowKey`主键Id
```html
<avue-crud :option="option" :data="data" >
  <template slot="icon" slot-scope="scope">
    <i :class="scope.row.icon" style="font-size:24px"></i>
  </template>
  <template slot="menu" slot-scope="scope">
   <el-button :size="scope.size" :type="scope.type">自定义内容</el-button>
  </template>
</avue-crud>
<script>
export default {
  data(){
    return {
       data: [
        {
          id: 0,
          event: '事件1',
          timeLine: 50,
          icon: 'el-icon-time',
          comment: '无'
        },
        {
          id: 1,
          event: '事件1',
          icon: 'el-icon-bell',
          timeLine: 100,
          comment: '无',
          children: [
            {
              id: 2,
              event: '事件2',
              icon: 'el-icon-star-on',
              timeLine: 10,
              comment: '无'
            },
            {
              id: 3,
              event: '事件3',
              timeLine: 90,
              comment: '无',
              children: [
                {
                  id: 4,
                  event: '事件4',
                  timeLine: 5,
                  comment: '无'
                },
                {
                  id: 5,
                  event: '事件5',
                  timeLine: 10,
                  comment: '无'
                },
                {
                  id: 6,
                  event: '事件6',
                  timeLine: 75,
                  comment: '无',
                  children: [
                    {
                      id: 7,
                      event: '事件7',
                      timeLine: 50,
                      comment: '无',
                      children: [
                        {
                          id: 71,
                          event: '事件71',
                          timeLine: 25,
                          comment: 'xx'
                        },
                        {
                          id: 72,
                          event: '事件72',
                          timeLine: 5,
                          comment: 'xx'
                        },
                        {
                          id: 73,
                          event: '事件73',
                          timeLine: 20,
                          comment: 'xx'
                        }
                      ]
                    },
                    {
                      id: 8,
                      event: '事件8',
                      timeLine: 25,
                      comment: '无'
                    }
                  ]
                }
              ]
            }
          ]
        }
      ],
      option: {
        headerAlign: 'center',
        align: 'center',
        border: true,
        index: true,
        defaultExpandAll:true,
        column: [
          {
            label: '事件',
            prop: 'event',
            align: 'left',
            width: 200
          },
          {
            label: 'ID',
            prop: 'id',
            dicData: [{
              label: '零',
              value: 0
            }, {
              label: '一',
              value: 1
            }, {
              label: '二',
              value: 2,
            }]
          },
          {
            label: '时间线',
            prop: 'timeLine'
          },
          {
            label: '自定义图标',
            prop: 'icon',
            slot: true,
          },
          {
            label: '备注',
            prop: 'comment'
          }
        ],
      }
  }
  }
}
</script>


```
:::


