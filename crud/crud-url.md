<script>
export default {
   data() {
    return {
      user: {},
      data: [{
        icon: "el-icon-platform-eleme",
        color: 'rgba(201, 37, 37, 1)',
        array: '1,2,3',
        video:'/cssthemes5/twts_236_rage/assets/images/background/header.jpg',
        href: 'https://www.baidu.com',
        string:'/cssthemes5/twts_236_rage/assets/images/background/header.jpg,/cssthemes5/twts_236_rage/assets/images/background/header.jpg',
        img: 'http://demo.cssmoban.com/cssthemes5/twts_236_rage/assets/images/background/header.jpg,http://demo.cssmoban.com/cssthemes5/twts_236_rage/assets/images/background/header.jpg'
      }],
      option: {
        align: 'center',
        menuAlign: 'center',
        border: true,
        column: [{
          label: '超链接',
          prop: 'href',
          type: 'url'
        }, {
          label: '图标',
          prop: 'icon',
          type: 'icon'
        }, {
          label: '颜色',
          prop: 'color',
          type: 'color'
        },
        {
          label: '单图',
          prop: 'video',
          type: 'upload',
          listType: 'picture-img',
          span: 24,
          propsHttp: {
            home:'http://demo.cssmoban.com',
          },
          tip: '只能上传jpg/png用户头像，且不超过500kb',
          action: '/imgupload'
        }, {
          label: '图片',
          prop: 'img',
          dataType: 'string',
          type: 'img'
        },{
            label: '图片上传组件',
            prop: 'string',
            dataType: 'string',
            type: 'upload',
            propsHttp: {
              home:'http://demo.cssmoban.com',
              res: 'data'
            },
            span: 24,
            listType: 'picture-card',
            tip: '只能上传jpg/png文件，且不超过500kb',
            action: '/imgupload'
          }, {
          label: '数组',
          prop: 'array',
          dataType: 'number',
          propsHttp: {
            home:'http://demo.cssmoban.com',
          },
          type: 'array'
        }]
      }
    }
  }
}
</script>

# URL/IMG/ARRAY/ICON/COLOR
- 图片和超链都是可以点击的哦
:::tip
 2.5.3+
::::


:::demo
```html
<avue-crud ref="crud" :option="option" :data="data"></avue-crud>

<script>
export default {
   data() {
    return {
      user: {},
      data: [{
        icon: "el-icon-platform-eleme",
        color: 'rgba(201, 37, 37, 1)',
        array: '1,2,3',
        video:'/cssthemes5/twts_236_rage/assets/images/background/header.jpg',
        href: 'https://www.baidu.com',
        string:'/cssthemes5/twts_236_rage/assets/images/background/header.jpg,/cssthemes5/twts_236_rage/assets/images/background/header.jpg',
        img: 'http://demo.cssmoban.com/cssthemes5/twts_236_rage/assets/images/background/header.jpg,http://demo.cssmoban.com/cssthemes5/twts_236_rage/assets/images/background/header.jpg'
      }],
      option: {
        align: 'center',
        menuAlign: 'center',
        border: true,
        column: [{
          label: '超链接',
          prop: 'href',
          type: 'url'
        }, {
          label: '图标',
          prop: 'icon',
          type: 'icon'
        }, {
          label: '颜色',
          prop: 'color',
          type: 'color'
        },
        {
          label: '单图',
          prop: 'video',
          type: 'upload',
          listType: 'picture-img',
          span: 24,
          propsHttp: {
            home:'http://demo.cssmoban.com',
          },
          tip: '只能上传jpg/png用户头像，且不超过500kb',
          action: '/imgupload'
        }, {
          label: '图片',
          prop: 'img',
          dataType: 'string',
          type: 'img'
        },{
            label: '图片上传组件',
            prop: 'string',
            dataType: 'string',
            type: 'upload',
            propsHttp: {
              home:'http://demo.cssmoban.com',
              res: 'data'
            },
            span: 24,
            listType: 'picture-card',
            tip: '只能上传jpg/png文件，且不超过500kb',
            action: '/imgupload'
          }, {
          label: '数组',
          prop: 'array',
          dataType: 'number',
          propsHttp: {
            home:'http://demo.cssmoban.com',
          },
          type: 'array'
        }]
      }
    }
  }
}
</script>
```
:::