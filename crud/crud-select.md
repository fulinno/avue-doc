<script>
var baseUrl = 'https://cli.avuejs.com/api/area'
export default {
  data(){
    return {
       data:[],
       option:{
          column: [{
            label: '省份',
            prop: 'province',
            type: 'select',
            props: {
              label: 'name',
              value: 'code'
            },
            cascaderItem: ['city', 'area'],
            search:true,
            dicUrl: `${baseUrl}/getProvince`,
            rules: [
              {
                required: true,
                message: '请选择省份',
                trigger: 'blur'
              }
            ]
          },
          {
            label: '城市',
            prop: 'city',
            type: 'select',
            props: {
              label: 'name',
              value: 'code'
            },
            search:true,
            dicUrl: `${baseUrl}/getCity/{{key}}`,
            rules: [
              {
                required: true,
                message: '请选择城市',
                trigger: 'blur'
              }
            ]
          },
          {
            label: '地区',
            prop: 'area',
            type: 'select',
            props: {
              label: 'name',
              value: 'code'
            },
            search:true,
            dicUrl: `${baseUrl}/getArea/{{key}}`,
            rules: [
              {
                required: true,
                message: '请选择地区',
                trigger: 'blur'
              }
            ]
          }]
       }
    }
  },
  created() {
    setTimeout(() => {
      this.data = [
        {
          id: 1,
          name: '张三',
          province: '110000',
          city: '110100',
          area: '110101',
        },
        {
          id: 2,
          name: '李四',
          province: '140000',
          city: '140600',
          area: '140623'
        }
      ]
    }, 0)
  },
  methods:{
    searchChange(form,done){
       setTimeout(()=>{
          this.$message.success(JSON.stringify(form))
         done()
       },500)
    }
  }
}
</script>

# 搜索多级联动
- 多个选择框分级联动
:::tip
 2.2.3+
::::

## 普通用法 
:::demo  `cascaderItem`为需要联动的子选择框`prop`值，注意删除key之间的空格,表格字典翻译数据必须是异步

```html
<avue-crud :option="option" :data="data" @search-change="searchChange" @submit="handleSubmit"></avue-crud>
<script>
var baseUrl = 'https://cli.avuejs.com/api/area'
export default {
  data(){
    return {
       data:[],
       option:{
          column: [{
            label: '省份',
            prop: 'province',
            type: 'select',
            props: {
              label: 'name',
              value: 'code'
            },
            cascaderItem: ['city', 'area'],
            search:true,
            dicUrl: `${baseUrl}/getProvince`,
            rules: [
              {
                required: true,
                message: '请选择省份',
                trigger: 'blur'
              }
            ]
          },
          {
            label: '城市',
            prop: 'city',
            type: 'select',
            props: {
              label: 'name',
              value: 'code'
            },
            search:true,
            dicUrl: `${baseUrl}/getCity/{{key}｝`,
            rules: [
              {
                required: true,
                message: '请选择城市',
                trigger: 'blur'
              }
            ]
          },
          {
            label: '地区',
            prop: 'area',
            type: 'select',
            props: {
              label: 'name',
              value: 'code'
            },
            search:true,
            dicUrl: `${baseUrl}/getArea/{{key}｝`,
            rules: [
              {
                required: true,
                message: '请选择地区',
                trigger: 'blur'
              }
            ]
          }]
       }
    }
  },
  created() {
    setTimeout(() => {
      this.data = [
        {
          id: 1,
          name: '张三',
          province: '110000',
          city: '110100',
          area: '110101',
        },
        {
          id: 2,
          name: '李四',
          province: '140000',
          city: '140600',
          area: '140623'
        }
      ]
    }, 0)
  },
  methods:{
    searchChange(form,done){
       setTimeout(()=>{
          this.$message.success(JSON.stringify(form))
         done()
       },500)
    }
  }
}
</script>

```
:::


