<script>
export default {
 data() {
      return {
        text: false,
        permission: {},
        option: {
          column: [{
            label: '姓名',
            prop: 'name'
          }, {
            label: '年龄',
            prop: 'sex'
          }]
        },
        option1: {
          editBtn:false,
          delBtn:false,
          column: [{
            label: '姓名',
            prop: 'name'
          }, {
            label: '年龄',
            prop: 'sex'
          }]
        },
        data: [{
          id: 1,
          name: '张三',
          sex: 12,
        }, {
          id: 2,
          name: '李四',
          sex: 20,
        }]
      }
    },
    watch: {
      text() {
        if (this.text === true) {
          this.permission = {
            delBtn: false,
            addBtn: false,
            menu:false,
          }
        } else {
          this.permission = {
            delBtn: true,
            addBtn: true,
            menu:true,
          }
        }
      }
    },
    methods: {
      getPermission(key, row, index){
        if(key==='editBtn' && index==0){
            return false;
        }else if(key==='delBtn' && index==1){
          return false;
        }
         return true;
      }
    }
}
</script>

# 表格权限控制
控制封装中的按钮权限
:::tip
 2.6.0+
::::

## 普通用法
:::demo 
```html
权限开关
<el-switch :active-value="false" :inactive-value="true" v-model="text" active-color="#13ce66" inactive-color="#ff4949">
</el-switch>
<avue-crud :option="option" :permission="permission" :data="data"></avue-crud>
<script>
export default {
 data() {
      return {
        text: false,
        permission: {},
        option: {
          column: [{
            label: '姓名',
            prop: 'name'
          }, {
            label: '年龄',
            prop: 'sex'
          }]
        },
        data: [{
          id: 1,
          name: '张三',
          sex: 12,
        }, {
          id: 2,
          name: '李四',
          sex: 20,
        }]
      }
    },
    watch: {
      text() {
        if (this.text === true) {
          this.permission = {
            delBtn: false,
            addBtn: false,
            menu:false,
          }
        } else {
          this.permission = {
            delBtn: true,
            addBtn: true,
            menu:true,
          }
        }
      }
    }
}
</script>

```
:::

## 函数用法
:::demo 
```html
<avue-crud :option="option" :permission="getPermission" :data="data"></avue-crud>
<script>
export default {
 data() {
      return {
        option: {
          column: [{
            label: '姓名',
            prop: 'name'
          }, {
            label: '年龄',
            prop: 'sex'
          }]
        },
        data: [{
          id: 1,
          name: '张三',
          sex: 12,
        }, {
          id: 2,
          name: '李四',
          sex: 20,
        }]
      }
    },
    methods: {
      getPermission(key, row, index){
        if(key==='editBtn' && index==0){
            return false;
        }else if(key==='delBtn' && index==1){
          return false;
        }
         return true;
      }
    }
}
</script>

```
:::

## 自定义用法
:::demo 
```html
<avue-crud :option="option1"  ref="crud" :data="data">
  <div slot="menu" slot-scope="{size,type,row,index}">
    <el-button :type="type" :size="size" v-if="index==0" @click="$refs.crud.rowEdit(row,index)">编辑</el-button>
    <el-button :type="type" :size="size">删除</el-button>
  </div>
</avue-crud>
<script>
export default {
 data() {
      return {
        option1: {
          editBtn:false,
          delBtn:false,
          column: [{
            label: '姓名',
            prop: 'name'
          }, {
            label: '年龄',
            prop: 'sex'
          }]
        },
        data: [{
          id: 1,
          name: '张三',
          sex: 12,
        }, {
          id: 2,
          name: '李四',
          sex: 20,
        }]
      }
    },
    methods: {
     
    }
}
</script>
```
:::

## Permission Attributes

|参数|说明|类型|可选值|默认值|
|----------------|-----------------------|--------------------|------------------|---------------------|
|viewBtn|查看按钮|Boolean|false / true|true|
|editBtn|编辑按钮|Boolean|false / true|true|
|addBtn|新增按钮|Boolean|false / true|true|
|addRowBtn|行新增按钮|Boolean|false / true|true|
|printBtn|打印按钮|Boolean|false / true|true|
|excelBtn|excel导出按钮|Boolean|false / true|true|
|delBtn|删除按钮|Boolean|false / true|true|
|cellBtn|行编辑按钮|Boolean|false / true|true|
|dateBtn|日期按钮|Boolean|false / true|true|
|refreshBtn|刷新按钮|Boolean|false / true|true|
|columnBtn|列显隐按钮|Boolean|false / true|true|
|filterBtn|过滤器按钮|Boolean|false / true|true|
|searchBtn|搜索按钮|Boolean|false / true|true|
|menuBtn|多功能菜单按钮|Boolean|false / true|true|

