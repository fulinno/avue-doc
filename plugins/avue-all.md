# avue全家桶

- [后台模板](/doc/plugins/avue-cli)
- [地图选择器](/doc/plugins/map-plugins)
- [富文本编辑器](/doc/plugins/ueditor-plugins)
- [表单设计器](https://form.avuejs.com)
- [表格设计器](https://crud.avuejs.com)