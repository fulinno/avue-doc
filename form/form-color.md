<script>
export default{
  data() {
    return {
      form: { 
        color:'rgba(42, 20, 20, 1)',
      },
      option: {
        labelWidth: 120,
        column: [
          {
            label: '颜色选择器',
            prop: 'color',
            type: 'color',
          }
        ]
      }
    }
  }
}
</script>

# 颜色选择器
:::tip
 1.0.10+
::::

## 普通用法 
:::demo  
```html
<avue-form :option="option" v-model="form" > </avue-form>

<script>
export default{
  data() {
    return {
      form: { 
        color:'rgba(42, 20, 20, 1)',
      },
      option: {
        labelWidth: 120,
        column: [
          {
            label: '颜色选择器',
            prop: 'color',
            type: 'color',
          }
        ]
      }
    }
  }
}
</script>

```
:::
