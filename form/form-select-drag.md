<script>
export default {
    data() {
      return {
        obj: {
         select:['Shanghai','Beijing','Shenzhen']
        },
        option: {
          column: [
            {
            label: '拖拽',
            prop: 'select',
            type: 'select',
            drag: true,
            multiple: true,
            dicData: [{
                value: 'Shanghai',
                label: '上海'
              }, {
                value: 'Beijing',
                label: '北京'
              }, {
                value: 'Shenzhen',
                label: '深圳'
              }]
            }
          ]
        }
      }
    }
}
</script>
# 表单选择框拖拽

:::tip
 2.1.0+
::::


```
<!-- 导入需要的包 -->
<script src="https://cdn.staticfile.org/Sortable/1.10.0-rc2/Sortable.min.js"></script>
```


:::demo  配置`drag`为`true`即可开启分组模式
```html
<avue-form :option="option" v-model="obj"></avue-form>
<script>
export default {
    data() {
      return {
        obj: {
         select:['Shanghai','Beijing','Shenzhen']
        },
        option: {
          column: [
            {
            label: '拖拽',
            prop: 'select',
            type: 'select',
            drag: true,
            multiple: true,
            dicData: [{
                value: 'Shanghai',
                label: '上海'
              }, {
                value: 'Beijing',
                label: '北京'
              }, {
                value: 'Shenzhen',
                label: '深圳'
              }]
            }
          ]
        }
      }
    }
}
</script>

```
:::

