<script>
let baseUrl = 'https://cli.avuejs.com/api/area';
export default {
  data(){
    return {
       form:{
          text:'',
       },
       option:{
          column: [{
            label: '字典',
            prop: 'text',
            props: {
              label: 'name',
              value: 'code'
            },
            dicData:[{
              name:'自定义字典',
              code:-1
            }], 
            dicUrl: `${baseUrl}/getProvince`,
            dicMethod:'get',
            dicQuery:{
              a:1
            },
            span:24,
            type:'select'
          }]
       }
    }
  },
  methods:{
    handleSubmit(form){
       this.$message.success(JSON.stringify(this.form))
    }
  }
}
</script>
# 网络字典
可以改变发送请求的方式和传入相关参数，目前只有get / post俩种，默认为get

## 普通用法 
:::demo  `dicMethod`为请求方式，`dicQuery`为需要传入的参数对象
```html
<avue-form :option="option" v-model="form" @submit="handleSubmit"></avue-form>
<script>
let baseUrl = 'https://cli.avuejs.com/api/area';
export default {
  data(){
    return {
       form:{
          text:'',
       },
       option:{
          column: [{
            label: '字典',
            prop: 'text',
            props: {
              label: 'name',
              value: 'code'
            },
            dicData:[{
              name:'自定义字典',
              code:-1
            }], 
            dicUrl: `${baseUrl}/getProvince`,
            dicMethod:'get',
            dicQuery:{
              a:1
            },
            span:24,
            type:'select'
          }]
       }
    }
  },
  methods:{
    handleSubmit(form){
       this.$message.success(JSON.stringify(this.form))
    }
  }
}
</script>

```
:::

