
## 国际化

Avue 组件内部默认使用中文，若希望使用其他语言，则需要进行多语言设置。以英文为例，在 main.js 中：

```javascript
// 完整引入英文国际化
import Vue from 'vue'
import Avue from 'Avue'

Vue.use(Avue, { locale:'en' })
```


## 兼容其他 i18n 插件
如果不使用 `vue-i18n@5.x`，而是用其他的 i18n 插件，Element 将无法兼容，但是可以自定义 Element 的 i18n 的处理方法。

```javascript
import Vue from 'vue'
import Avue from 'Avue'

Vue.use(Element, {
  i18n: function (path, options) {
    // ...
  }
})
```


目前 Avue 内置了中文和英文不是很完整

如果你需要使用其他的语言或则充实语言，欢迎贡献 PR：只需在 [这里](https://git.avuejs.com/avue/avuex/src/master/src/locale/lang) 添加一个语言配置文件即可。