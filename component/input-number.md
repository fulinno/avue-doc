<script>
export default {
    data() {
      return {
        form:1,
      }
    }
}
</script>



## inputNumber数字框

:::demo 
```html
<el-row :span="24">
  <el-col :span="6">
   值:{{form}}<br/>
   <avue-input-number v-model="form" ></avue-input-number>
  </el-col>
  <el-col :span="24"></el-col>
  <el-col :span="6">
   值:{{form}}<br/>
    <avue-input-number v-model="form" precision="2" controls-position=""></avue-input-number>
  </el-col>
</el-row>

<script>
export default {
    data() {
      return {
        form:1
      }
    }
}
</script>

```
:::