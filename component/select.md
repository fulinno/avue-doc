<script>
export default {
    data() {
      return {
        form:'',
        form1:[0,1],
        dic:[{
          label:'选项1',
          value:0
        },{
          label:'选项2',
          value:1
        }]
      }
    }
}
</script>



## select选择框

:::demo 
```html
<el-row :span="24">
  <el-col :span="6">
     值:{{form}}<br/>
     <avue-select v-model="form" placeholder="请选择内容" type="tree" :dic="dic"></avue-select>
  </el-col>
   <el-col :span="24"></el-col>
   <el-col :span="6">
     值:{{form1}}<br/>
     <avue-select multiple v-model="form1" placeholder="请选择内容" type="tree" :dic="dic"></avue-select>
  </el-col>
</el-row>
<script>
export default {
    data() {
      return {
        form:'',
        form1:[0,1],
        dic:[{
          label:'选项1',
          value:0
        },{
          label:'选项2',
          value:1
        }]
      }
    }
}
</script>

```
:::