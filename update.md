# #PigX-ui 和 saber 升级成企业版 avuex
感谢亡羊补牛(858695266)提供
## PigX-ui 升级avuex

### 以下升级是最新pigx 2.7.0版本UI

- 1、项目代码全文搜索 solt 替换成 slot
- 2、avue-crud-* 替换成 avue-*，去掉中间的crud
- 3、最后一步改名替换，修改avuex源码目录下的： 
>lib/avue.min.js 为index.js

用改名后的
>index.js 和 index.css 

替换掉 
>pigx-ui/public/cdn/avue/ 下的同名文件

* 注意，尽量替换后覆盖文件，因为avuex源码里有 avue-crud-\* 相关组件
  
## PigX-ui 增加主题

pigx-ui/src/page/index/top/top-theme.vue 第48行增加如下代码：

```bash
           ,
          {
            name: "iview主题",
            value: "theme-iview"
          },
          {
            name: "d2主题",
            value: "theme-d2"
          },
          {
            name: "hey主题",
            value: "theme-hey"
          }
```
pigx-ui/src/styles/theme/index.scss 增加如下代码：

```bash
// d2主题
@import './d2.scss';

//iview
@import './iview.scss';

//heyui
@import './hey.scss';
```
下载saber的theme样式文件，复制到pigx-ui/src/styles/theme/ 文件夹下，下载地址如下：
https://gitee.com/smallc/Saber/tree/master/src/styles/theme

只需要：
>d2.scss
hey.scss
iview.scss

效果图就去
[http://saber.avuex.top ](https://saber.avuejs.com/#/login)
自己看看吧！

## Saber 升级

没什么可说了，皮肤都是saber来的，直接替换public/cdn/avue目录下的文件就可以了。