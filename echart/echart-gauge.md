<script>
echarts.registerTheme(theme.themeName, theme.theme)
echarts.registerTheme(theme1.themeName, theme1.theme)
echarts.registerTheme(theme2.themeName, theme2.theme)
export default {
  data() {
      return {
        theme:'',
        data: {
          value: 50,
          name: '完成率',
          min: 0,
          max: 100,
          unit: '%'
        },
        config: {
          switchTheme:true,
          lineSize: 25,
          barColor: [{
            postion: 0.2,
            color1: '#91c7ae'
          },
          {
            postion: 0.8,
            color1: '#63869e'
          },
          {
            postion: 1,
            color1: '#c23531'
          }
          ]
        }
      }
    },
}
</script>>


# EchartGauge 刻度盘
:::tip
1.1.0+
::::

```
<!-- 导入需要的包 -->  
<script src="https://cdnjs.cloudflare.com/ajax/libs/echarts/4.2.1/echarts.min.js"></script>
<script src="https://avuejs.com/theme/avue.project.js"></script>
<script src="https://avuejs.com/theme/halloween.project.js"></script>
<script src="https://avuejs.com/theme/wonderland.project.js"></script>
```

:::demo 
```html
<el-button type="primary" @click="config.switchTheme=!config.switchTheme" size="small">{{config.switchTheme?'关闭主题':'打开主题'}}</el-button>
<el-button @click="theme='macarons'" size="small">换紫色主题</el-button>
<el-button @click="theme='wonderland'" size="small">换绿色主题</el-button>
<avue-echart-gauge ref="echart" :theme="theme" :option="config" :data="data" width="1000"></avue-echart-gauge>
<script>
echarts.registerTheme(theme.themeName, theme.theme)
echarts.registerTheme(theme1.themeName, theme1.theme)
echarts.registerTheme(theme2.themeName, theme2.theme)
export default {
  data() {
      return {
        theme:'',
        data: {
          value: 50,
          name: '完成率',
          min: 0,
          max: 100,
          unit: '%'
        },
        config: {
          switchTheme:true,
          lineSize: 25,
          barColor: [{
            postion: 0.2,
            color1: '#91c7ae'
          },
          {
            postion: 0.8,
            color1: '#63869e'
          },
          {
            postion: 1,
            color1: '#c23531'
          }
          ]
        }
      }
    },
}
</script>

```
:::




