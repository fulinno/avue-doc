<script>
echarts.registerTheme(theme.themeName, theme.theme)
echarts.registerTheme(theme1.themeName, theme1.theme)
echarts.registerTheme(theme2.themeName, theme2.theme)
export default {
  data() {
      return {
        theme:'',
        data: {
          indicator: [{
              name: '销售',
              max: 6500
            },
            {
              name: '管理',
              max: 16000
            },
            {
              name: '信息技术',
              max: 30000
            },
            {
              name: '客服',
              max: 38000
            },
            {
              name: '研发',
              max: 52000
            },
            {
              name: '市场',
              max: 25000
            }
          ],
          series: [{
            data: [{
                value: [4300, 10000, 28000, 35000, 50000, 19000],
                name: '预算分配（Allocated Budget）'
              },
              {
                value: [5000, 14000, 28000, 31000, 42000, 21000],
                name: '实际开销（Actual Spending）'
              }
            ]
          }]
        },
        option: {
          switchTheme:true,
          width: '100%',
          height: 600,
          title: '各部门开销',
          subtext: '最近一个月',
          titleColor: '#666',
          shape: 'polygon',
          radius: "75%",
          splitNumber: 4,
          radarNameColor: '#333',
          radarNameSize: 12,
          titleFontSize: 18,
          legendShow: true,
          labelShow: true,
          "barColor": [{
              "color1": "#83bff6",
            },
            {
              "color1": "#23B7E5",
            },
            {
              "color1": "rgba(154, 168, 212, 1)",
            },
            {
              "color1": "#188df0",
            },
            {
              "color1": "#564AA3",
            }
          ]
        },
      }
    },
}
</script>


# EchartRadar 雷达图
:::tip
2.0.1+
::::

```
<!-- 导入需要的包 -->  
<script src="https://cdnjs.cloudflare.com/ajax/libs/echarts/4.2.1/echarts.min.js"></script>
<script src="https://avuejs.com/theme/avue.project.js"></script>
<script src="https://avuejs.com/theme/halloween.project.js"></script>
<script src="https://avuejs.com/theme/wonderland.project.js"></script>
```

:::demo 
```html
<el-button type="primary" @click="option.switchTheme=!option.switchTheme" size="small">{{option.switchTheme?'关闭主题':'打开主题'}}</el-button>
<el-button @click="theme='macarons'" size="small">换紫色主题</el-button>
<el-button @click="theme='wonderland'" size="small">换绿色主题</el-button>
<avue-echart-radar  :theme="theme" :option="option" :data="data" :width="1000"></avue-echart-radar>
<script>
echarts.registerTheme(theme.themeName, theme.theme)
echarts.registerTheme(theme1.themeName, theme1.theme)
echarts.registerTheme(theme2.themeName, theme2.theme)
export default {
  data() {
      return {
        theme:'',
        data: {
          indicator: [{
              name: '销售',
              max: 6500
            },
            {
              name: '管理',
              max: 16000
            },
            {
              name: '信息技术',
              max: 30000
            },
            {
              name: '客服',
              max: 38000
            },
            {
              name: '研发',
              max: 52000
            },
            {
              name: '市场',
              max: 25000
            }
          ],
          series: [{
            data: [{
                value: [4300, 10000, 28000, 35000, 50000, 19000],
                name: '预算分配（Allocated Budget）'
              },
              {
                value: [5000, 14000, 28000, 31000, 42000, 21000],
                name: '实际开销（Actual Spending）'
              }
            ]
          }]
        },
        option: {
          switchTheme:true,
          width: '100%',
          height: 600,
          title: '各部门开销',
          subtext: '最近一个月',
          titleColor: '#666',
          shape: 'polygon',
          radius: "75%",
          splitNumber: 4,
          radarNameColor: '#333',
          radarNameSize: 12,
          titleFontSize: 18,
          legendShow: true,
          labelShow: true,
          "barColor": [{
              "color1": "#83bff6",
            },
            {
              "color1": "#23B7E5",
            },
            {
              "color1": "rgba(154, 168, 212, 1)",
            },
            {
              "color1": "#188df0",
            },
            {
              "color1": "#564AA3",
            }
          ]
        },
      }
    },
}
</script>

```
:::




